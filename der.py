from os import listdir
from datetime import datetime, date
import matplotlib.pyplot as plt
import pandas
import matplotlib.dates as mdates


# Reading data and cleaning it.
data_frames = [pandas.read_csv(f'csv_datas/{fn}', encoding='utf-16') for fn in listdir('csv_datas')]
_data_frames = []
for df in data_frames:
    # df['<DTYYYYMMDD>'] = [datetime.strptime(str(date), '%Y%m%d').strftime('%Y-%m-%d') for date in df['<DTYYYYMMDD>']]
    df.drop(columns=['<LOW>', '<HIGH>', '<OPEN>', '<OPENINT>', '<OPENINT>.1', '<OPENINT>.2'], inplace=True)
    df.rename(columns={'<TICKER>': 'ticker', '<DTYYYYMMDD>': 'date', '<CLOSE>': 'close', '<VOL>': 'volume'}, inplace=True)
    df['date'] = [datetime.strptime(str(datee), '%Y%m%d') for datee in df['date']]

    _df = df[['ticker', 'date',]]
    _close = df['close'].iloc[1:].rename(lambda x: x-1) - df['close'].iloc[:-1]
    _volume = df['volume'].iloc[1:].rename(lambda x: x-1) - df['volume'].iloc[:-1]
    _df = _df.assign(close=_close, volume=_volume)
    _data_frames.append(_df)

del _df, _close, _volume, data_frames, df


# Making Axis and Deleting Zero Volume Records
n = 2
_from = 400
_to = 10000000
c = 100000
dates, vols, closes = [], [], []
for index, row in _data_frames[n].iloc[_from:].iterrows():
    if index > _to:
        break
    if row['volume'] != 0:
        vols.append(row['volume']/c)
        closes.append(row['close'])
        dates.append(row['date'])


# Plotting
fig, ax = plt.subplots()

ax.plot(dates, vols, label='Volume')
ax.plot(dates, closes, label='Close', alpha=1, linewidth=0.8)
# ax.plot(dates, another, label='another')


# Formatting ticks
ax.xaxis.set_major_locator(mdates.YearLocator())
ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y'))
ax.xaxis.set_minor_locator(mdates.MonthLocator())

datemin = date(_data_frames[n]['date'].min().year, 1, 1)
datemax = date(_data_frames[n]['date'].max().year+1, 1, 1)
ax.set_xlim(datemin, datemax)
# ax.axes.get_xaxis().set_visible(False)


fig.autofmt_xdate()
ax.set_title(_data_frames[n]['ticker'][0])
ax.legend()
plt.xticks(rotation=90)
plt.show()
