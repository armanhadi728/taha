from os import listdir
from datetime import datetime, date
import matplotlib.pyplot as plt
import pandas
import matplotlib.dates as mdates


# Reading data and cleaning it.
data_frames = [pandas.read_csv(f'csv_datas/{fn}', encoding='utf-16') for fn in listdir('csv_datas')]
for df in data_frames:
    # df['<DTYYYYMMDD>'] = [datetime.strptime(str(date), '%Y%m%d').strftime('%Y-%m-%d') for date in df['<DTYYYYMMDD>']]
    df['<DTYYYYMMDD>'] = [datetime.strptime(str(datee), '%Y%m%d') for datee in df['<DTYYYYMMDD>']]


# Making Axis and Deleting Zero Volume Records
n = 2
m = 10000000
c = 10000
dates, vols, closes = [], [], []
another = []
for index, row in data_frames[n].iterrows():
    if index > m:
        break
    if row['<VOL>'] != 0:
        vols.append(row['<VOL>']/c)
        closes.append(row['<CLOSE>'])
        another.append(row['<HIGH>'])
        dates.append(row['<DTYYYYMMDD>'])


# Plotting
fig, ax = plt.subplots()

ax.plot(dates, vols, label='Volume')
ax.plot(dates, closes, label='Close')
# ax.plot(dates, another, label='another')


# Formatting ticks
ax.xaxis.set_major_locator(mdates.YearLocator())
ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y'))
ax.xaxis.set_minor_locator(mdates.MonthLocator())

datemin = date(data_frames[n]['<DTYYYYMMDD>'].min().year, 1, 1)
datemax = date(data_frames[n]['<DTYYYYMMDD>'].max().year+1, 1, 1)
ax.set_xlim(datemin, datemax)
# ax.axes.get_xaxis().set_visible(False)


fig.autofmt_xdate()
ax.set_title(data_frames[n]['<TICKER>'][0])
ax.legend()
plt.xticks(rotation=90)
plt.show()
