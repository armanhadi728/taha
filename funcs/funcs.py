from os import listdir
from datetime import datetime, date
import matplotlib.pyplot as plt
import pandas
import matplotlib.dates as mdates
from numpy import linspace
import gc


def graph_decarator(func):
        def wrapper(self, dfs, *args, save=None, legends=('volume', 'close'), _from=0, _to=0, **kwargs):
            dfs = dfs[_from:_to] if _to else dfs[_from:]
            for i, df in enumerate(dfs):
                fig = self.befores()
                ax = fig.subplots()
                for legend in legends:
                    func(dfs, save, legends, _from, _to, ax=ax, df=df, legend=legend, **kwargs)
                ax.legend()
                if save is not None:
                    plt.savefig(f'{save}/{i}_{df.sample().ticker.values[0]}.png')
                    plt.close(fig=fig)
            if save is None:
                plt.show()

        return wrapper


class MyData:
    def __init__(self, dir='csv_datas', size=(1600, 900), cut=False, normalize=False):
        files = [pandas.read_csv(f'{dir}/{fn}', encoding='utf-16') for fn in listdir(dir)]
        self.size = size

        self.data_frames = []
        self._data_frames = []
        for df in files:
            df.drop(columns=['<LOW>', '<HIGH>', '<OPEN>', '<OPENINT>', '<OPENINT>.1', '<OPENINT>.2'], inplace=True)
            df.rename(columns={'<TICKER>': 'ticker', '<DTYYYYMMDD>': 'date', '<CLOSE>': 'close', '<VOL>': 'volume'}, inplace=True)
            df['date'] = [datetime.strptime(str(datee), '%Y%m%d') for datee in df['date']]

            df.drop(df[df.volume == 0].index, inplace=True)
            df.drop(df[df.volume == 'nan'].index, inplace=True)

            if len(df.index) < 400:
                continue

            _df = df[['ticker', 'date',]]
            _close = df['close'].iloc[1:].rename(lambda x: x-1) - df['close'].iloc[:-1]
            _volume = df['volume'].iloc[1:].rename(lambda x: x-1) - df['volume'].iloc[:-1]
            _df = _df.assign(close=_close, volume=_volume)

            if cut:
                indc = _df.index[abs(_df['volume']) > 150000]
                df.drop(index=indc, inplace=True)
                _df.drop(index=indc, inplace=True)

            if normalize:
                df.volume *= 0.0001
                # mean = df.volume.mean()
                # std = df.volume.std()
                # df = df[(mean-std >= df.volume) | (df.volume >= mean+std)]
                # if len(df.index) < 100:
                #     continue

            df.set_axis([i for i in range(len(df.index))], axis='index', inplace=True)
            _df.set_axis([i for i in range(len(_df.index))], axis='index', inplace=True)

            self._data_frames.append(_df)
            self.data_frames.append(df)

        del _df, _close, _volume, df, files

    def befores(self):
        dpi = 100
        fig = plt.figure(figsize=(self.size[0]/dpi, self.size[1]/dpi), dpi=dpi)
        plt.subplots_adjust(left=0.04, bottom=0.05, right=0.990, top=0.960, wspace=0, hspace=0.1)
        return fig

    def sort(self):
        self.data_frames_normal = []
        self._data_frames_normal = []
        for df, _df in zip(self.data_frames, self._data_frames):
            self.data_frames_normal.append(df.sort_values(by=['volume', 'close']))
            self._data_frames_normal.append(_df.sort_values(by=['volume', 'close']))
        return self.data_frames_sort, self._data_frames_sort

    def cost(self, df, v='volume', c='close', a1=1, a2=0, b1=1, b2=0):
        ch = pandas.Series(dtype='float64')
        ch = (df[v].rename(lambda i: round(b1*i + b2)) * a1) + a2

        _min = min([df.index[0], df.index[-1], ch.index[0], ch.index[-1]])
        _max = max([df.index[0], df.index[-1], ch.index[0], ch.index[-1]])
        # print(df.index[0], df.index[-1], ch.index[0], ch.index[-1])
        m = _max - _min + 1

        e = (1/(2*m))*((ch.sub(df[c], fill_value=0) ** 2).sum())
        del _min, _max, m, ch
        return e


    def gradient(self, df, v='volume', c='close', a1=1, a2=0, b1=1, b2=0):
        ch = pandas.Series(dtype='float64')
        _v = df[v].rename(lambda i: round(b1*i + b2))
        dv = _v.iloc[1:] - _v.iloc[:-1].rename(lambda x: x+1)
        ch = (_v * a1) + a2
        diff = ch.sub(df[c], fill_value=0)
        # print(diff)
        # print(_v)
        # print(dv)

        _min = min([df.index[0], df.index[-1], ch.index[0], ch.index[-1]])
        _max = max([df.index[0], df.index[-1], ch.index[0], ch.index[-1]])
        m = _max - _min + 1
        # print(df.index[0], df.index[-1], ch.index[0], ch.index[-1])
        ll = list(range(_min, _max+1))
        mlist = pandas.Series(ll, ll)

        da1 = (1/m) * (diff.multiply(_v, fill_value=0)).sum()
        da2 = (1/m) * (diff).sum()
        d = diff.multiply(dv, fill_value=0).multiply(mlist, fill_value=0)
        db1 = (1/m) * (d).sum()
        print(db1, d.sum())
        del d
        db2 = (1/m) * (diff.multiply(dv, fill_value=0)).sum()

        del _min, _max, m, ll, mlist, ch, _v, dv, diff

        return pandas.Series([da1, da2, db1, db2])

    def cost2(self, df, v='volume', c='close', a1=1, a2=0):
        ch = pandas.Series(dtype='float64')
        ch = (df[v] * a1) + a2

        _min = min([df.index[0], df.index[-1], ch.index[0], ch.index[-1]])
        _max = max([df.index[0], df.index[-1], ch.index[0], ch.index[-1]])
        m = _max - _min + 1

        e = (1/(2*m))*((ch.sub(df[c], fill_value=0) ** 2).sum())
        del _min, _max, m, ch
        return e

    def gradient2(self, df, v='volume', c='close', a1=1, a2=0):
        ch = pandas.Series(dtype='float64')
        _v = df[v]
        ch = (_v * a1) + a2
        diff = df[c].sub(ch, fill_value=0)
        print(diff)
        print(_v)

        _min = min([df.index[0], df.index[-1], ch.index[0], ch.index[-1]])
        _max = max([df.index[0], df.index[-1], ch.index[0], ch.index[-1]])
        m = _max - _min + 1
        ll = list(range(_min, _max+1))
        mlist = pandas.Series(ll, ll)

        da1 = (1/m) * (diff.multiply(_v, fill_value=0)).sum()
        da2 = (1/m) * (diff).sum()

        del _min, _max, m, ll, mlist, ch, _v, diff

        return pandas.Series([da1, da2,])

    def gradient_descent(self, df, cost_f, grad_f, start=[1,0,1,0], learn_rate=1, max_iter=1000, momentum=1, threshold=0.05):
        del self._data_frames
        steps = []
        x = pandas.Series(start)
        diff = pandas.Series([0,0,0,0])

        for _ in range(max_iter):
            gc.collect()
            print(x.tolist())
            grad = grad_f(df, a1=x[0], a2=x[1])
            diff = -learn_rate * grad + momentum * diff
            x = x.add(diff, fill_value=0)
            # print(grad.tolist())

            gc.collect()
            # cost = cost_f(df, a1=x[0], a2=x[1])
            # steps.append(cost)
            # if cost < threshold:
            #     break
            print('------------------------')

        return x

    @graph_decarator
    def plot(self, dfs : list, save=None, legends=('volume', 'close'), _from=0, _to=0, **kwargs):
        ax = kwargs.pop('ax')
        df = kwargs.pop('df')
        legend = kwargs.pop('legend')
        ax.plot(df['date'].to_list(), df[legend].to_list(), label=legend)

    @graph_decarator
    def histogram(self, dfs : list, save=None, legends=('volume', 'close'), _from=0, _to=0, **kwargs):
        ax = kwargs.pop('ax')
        df = kwargs.pop('df')
        legend = kwargs.pop('legend')
        ax.hist(df[legend].to_list(), label=legend, **kwargs)
        mean = df[legend].mean()
        std = df[legend].std()
        _df = df[(mean-std <= df[legend]) & (df[legend] <= mean+std)]
        count = (len(_df.index) / len(df.index)) * 100

        txt = """
        mean = {:.2E}
        std = {:.2E}
        data in = %{:.2f}
        """.format(mean, std, count)
        ax.text(1000000, 60, txt, bbox={'facecolor': 'red', 'alpha': 0.5, 'pad': 3})

        ax.axvline(mean, label=f'{legend} mean', color='r')
        ax.axvline(mean+std, color='y', linestyle='--')
        ax.axvline(mean-std, color='y', linestyle='--')
