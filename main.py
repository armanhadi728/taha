from funcs.funcs import MyData


d = MyData(dir='csv_datas', cut=False, normalize=True)
# d.sort()

n = 2
_from = n
_to = n+1

# d.plot(d.data_frames, save=None, legends=('close', 'volume'), _from=_from, _to=_to)
# d.plot(d.data_frames, save='figs_normalize', legends=('volume',))

res = d.gradient_descent(
    d.data_frames[n],
    d.cost2,
    d.gradient2,
    start=[1, 0, 1, 0],
    learn_rate=0.5,
    momentum=0,
    threshold=200
)
print(res)


# d.histogram(d.data_frames, save=None, legends=('volume',), _from=_from, _to=_to, bins=30, density=False, histtype='bar')
# d.histogram(d.data_frames, save='hists', legends=('volume',), bins=30, density=False, histtype='bar')
